# coding=utf-8
__author__ = 'fish'
import os
import sys


class Enrol(object):
    def __init__(self, file_location):
        # Argument passed is the path to enrolment files/details
        # Read them to class variables here
        self.file_location = file_location
        self.data_map = {
            "CLASSES": self._read_file("CLASSES"),
            "SUBJECTS": self._read_file("SUBJECTS"),
            "VENUES": self._read_file("VENUES"),
        }
        self.class_roll = self._generate_class_roll()

    def subjects(self):
        # Return a list of all subject codes in enrolment system
        return self.data_map.get("SUBJECTS").keys()

    def subject_name(self, subject_code):
        # Returns a string which is the subject name of the specified subject_code
        if self.data_map.get("SUBJECTS").get(subject_code):
            return str(self.data_map.get("SUBJECTS").get(subject_code))

        raise KeyError("Subject Code not found in Subjects dictionary")

    def classes(self, subject_code):
        # Return a list of class codes for the specified subject_code
        if self.data_map.get("SUBJECTS").get(subject_code):
            return [str(key) for key, value in self.data_map.get("CLASSES").iteritems() if value[0] == subject_code]

        raise KeyError("Subject Code not found in Subjects dictionary")

    def class_info(self, class_code):
        # Return class information in a tuple of the form (subjectcode, time, venue, tutor, students)
        _class_data = self.data_map.get("CLASSES").get(class_code)
        if _class_data:
            return tuple(_class_data + [self.class_roll.get(class_code)])

        raise KeyError("Class Code not found in Classes dictionary")

    def check_student(self, student_id, subject_code=None):
        if subject_code:
            # return class code of the class the student is enrolled in for subject specified by class code
            # look through CLASSES list & compare subject codes. If we get a match, check class code for student id.
            # If the class code holds our student id, return it
            for class_code, class_data in self.data_map.get("CLASSES").iteritems():
                if class_data[0] == subject_code:
                    for id in self.class_roll.get(class_code):
                        if id == student_id:
                            return class_code

            return None
        else:
            # return a list of class codes that the student is enrolled in across all possible subjects
            return [str(key) for key, value in self.class_roll.iteritems() if [id for id in value if id == student_id]]

    def enrol(self, student_id, class_code):
        # If the student is already enrolled in a class (including the specified class to be enrolled into) in the same
        # subject as the specified class, the student is removed from the current enrolled class before being placed
        # into the new one (i.e. the one specified by the second argument).
        classes = self.data_map.get("CLASSES")
        if not classes.get(class_code):
            raise KeyError("Class Code not found in Classes dictionary")

        subject_code = classes.get(class_code)[0]
        venue = classes.get(class_code)[2]
        venue_capacity = self.data_map.get("VENUES").get(venue)[0]
        num_enrolled = len(self.class_roll.get(class_code))

        # Check number of students in class is less than the capacity of the venue of the class
        if num_enrolled < venue_capacity:
            # Check to see if the student is enrolled in any classes that come under the subject
            for key, value in classes.iteritems():
                if value[0] == subject_code:
                    for student in self.class_roll.get(key):
                        if student == student_id:
                            self.class_roll.get(key).remove(student)

            # Add student to class specified
            self.class_roll.get(class_code).append(student_id)
            return 1

        return None

    def _read_file(self, filename):
        input_file = open(self.file_location + "\\" + filename)
        try:
            return dict([(field[0], field[1:]) for field in
                         [line.split(":") for line in input_file.read().splitlines() if line[0] != '#']])
        finally:
            input_file.close()

    def _generate_class_roll(self):
        class_roll = {}
        # use class keys as filenames for roll files, class key is key, file contents as values
        for key in self.data_map.get("CLASSES").keys():
            input_file = open(self.file_location + "\\" + key + ".roll")
            try:
                class_roll[key] = input_file.read().splitlines()
            finally:
                input_file.close()

        return class_roll


class Subject(object):
    def __init__(self, code, name):
        self.code = code
        self.name = name


class Class(object):
    def __init__(self, class_code, subject_code, start_time, venue, teacher):
        self.class_code = class_code
        self.subject_code = subject_code
        self.start_time = start_time
        self.venue = venue
        self.teacher = teacher

    def __str__(self):
        return self.class_code + ":" + self.subject_code + ":" + self.start_time + ":" + self.venue + ":" + self.teacher


class Venue(object):
    def __init__(self, name, capacity):
        self.name = name
        self.capacity = capacity


def read_lines(filename):
    # Use list comprehension to gather our values into a list. We use join to connect our values together with a ':'
    # delimiter. Map is used to convert all our values to a string
    return [(str(key) + ":" + ':'.join(map(str, value))) for key, value in enrol.data_map.get(filename).iteritems()]


def read_tables(filename):
    pass


def write_lines(filename, lines):
    pass


enrol = Enrol(os.getcwd())
subjects = enrol.subjects()
print "subjects() function returns a " + str(type(subjects)) + " of " + str(type(subjects[0]))
print "subject_name() function returns a " + str(type(enrol.subject_name("scr101")))
classes = enrol.classes("scr101")
print "classes() function returns a " + str(type(classes)) + " of " + str(type(classes[0]))
class_info = enrol.class_info("scr101.1")
print "class_info() function returns a " + str(type(class_info)) + " of " + str(type(class_info[0])) + " with " + str(
    type(class_info[-1]))
#print enrol.check_student("s1109202")
#print enrol.check_student("s1136607")
#print enrol.check_student("s1136608")
#print enrol.check_student("s1136607", "scr202")

enrol.enrol("s1109202", "scr101.1")

